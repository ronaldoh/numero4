/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incricao.convert;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author ronaldo
 */
@FacesConverter("cpfConverter")
public class CPFConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        try {
            Long.parseLong(o.toString());
            return o.toString();
        } catch (NumberFormatException e) { // CPF não possui somente números
            String msg = "CPF " + o.toString() + " inválido";
            FacesMessage mensagem = new FacesMessage(msg);
            throw new ValidatorException(mensagem);
        }

    }

}
